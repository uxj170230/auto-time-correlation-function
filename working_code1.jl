gamma_m =parse(Float64,ARGS[1])
dt = parse(Float64,ARGS[2])
t_max = parse(Float64,ARGS[3])
tdt = parse(Float64,ARGS[4])
N =parse(Int,ARGS[5])
h =parse(Float64,ARGS[6])
j = parse(Float64,ARGS[7])
hf =parse(Float64,ARGS[8])
jf = parse(Float64,ARGS[9])
#h=1-h
jf=j
hf=h
hnn=0.0
using SparseArrays
using NPZ
using LinearAlgebra
using DifferentialEquations
t_dt=collect(0:dt:tdt)
t_vals=collect(0:dt:t_max);


######################### Free Fermion Part ##########################




Hh=zeros(N,N)
Hj=zeros(N,N)
Hd=zeros(N,N)
Hh=zeros(N,N)
Hj=zeros(N,N)
Hnn=zeros(N,N)
for i=1:1:N-2
Hnn[i,i+2]=hnn/2
end
for i=1:2:N
    Hh[i,i+1]=h/2
end
for i=1:2:N-3
    Hj[i,i+3]=j/2
end
for  i=3:1:N
    Hd[1,1]=-2
    Hd[i,i]=-2
end
H1=Hj-Hj'+Hh-Hh'+Hnn-Hnn'; #H1 is the initial Hamiltonian in lab basis before the Quench.
H1=im*H1
D,U=eigvals(real((H1^2))),eigvecs(real((H1^2)));
sigma=U'*imag.(H1)*U; 
V1=Matrix(1.0*I,N,N)
for i in 2:2:N
    if sigma[i-1,i] < 0
        V1[i,i]=-1
    end
end
Q=U*V1
sigma_corr=Q'*H1*Q;
FCr=zeros(N,N);
FCr=complex(FCr)
for i=1:2:N
    FCr[i,i+1]=-im
    end    
FrCr=FCr+FCr'; # FrCr is the Correlation function in rotated basis before the Quench
FreeCr=Q*FrCr*Q'+Matrix(1.0*I,N,N);
for i=1:2:N
    Hh[i,i+1]=hf/2
end
for i=1:2:N-3
    Hj[i,i+3]=jf/2
end
# xdata=[]
#  for r=1:length(gamma)
    H1=Hh-Hh'+Hj-Hj'+Hnn-Hnn'; # H1f is the Hamiltonian in the lab basis after the Quench
    H1=im*H1;
    H1=sparse(H1)
#     g=gamma[r]
 g=gamma_m
    H1f=4*im*H1+Hd*g;

 #   Dt,Ut=eigvals(real.(copy(H1f))),eigvecs(real.(copy(H1f)));
 #  Dl,Ul=eigvals(real.(copy(transpose(H1f)))),eigvecs(real.(copy(transpose(H1f))));

  #   if g>0
   #     U1=0*Ul
    #    for n=1:N

     #       U1[:,n]=Ul[:,(findmax(abs.(Ul[:,n]'*Ut))[2][2])]

      #  end
       # L=inv(round.((U1'*Ut),digits=6))*(U1');
       # R=Ut
    # else
     #    R=Ut
      #   L=Ut';
    # end
   
     #diag(R*L)
    #println(maximum(abs.(H1f-R*diagm(Dt)*L)))


        row=1
        t=t_max

         
         

    x1f=zeros(length(t_dt),1)
    x1f=complex(x1f)

    using DifferentialEquations
    A=sparse(im*4*H1)
    P=spzeros(N,N)
    P[2,2]=1
    # println(row)
FreeCr2=copy(FreeCr)

    for m=1:length(t)
    global FreeCr2
        FreeCr1=FreeCr2
        #FreeCr2=complex(FreeCr2)
        tspan = (0.0,t)
        f(C,p,t) = -(-(A*C-C*A)+2*g*(-2*P*C*P+P*C+C*P))
        prob = ODEProblem(f,FreeCr2,tspan)
  #      sol=solve(prob,reltol=1e-6)
sol=solve(prob)     
    FreeCr2=sol[length(sol)]
        for n=1:(length(t_dt))
            x1f[n,m]=FreeCr1[2,2]

            FreeCr1=(exp(H1f*dt)*copy(FreeCr1))
#            FreeCr1=R*(exp(real.(diagm(0 => Dt))*dt))*L*copy(FreeCr1)

        end
    end
    npzwrite("tac_$(N)_$(g)_$(j)_$(h)_$(hf)_$(tdt)_$(dt)_$(t_max)",x1f);

