gamma_m =parse(Float64,ARGS[1])
dt = parse(Float64,ARGS[2])
t_max = parse(Float64,ARGS[3])
tdt = parse(Float64,ARGS[4])
N =parse(Int,ARGS[5])
h =parse(Int,ARGS[6])
j = parse(Float64,ARGS[7])
hf =parse(Int,ARGS[8])
jf = parse(Float64,ARGS[9])

using SparseArrays
using NPZ
using LinearAlgebra


using NPZ
using LinearAlgebra
using DifferentialEquations
using SparseArrays

# Nu=400
# t_max=5.
# tdt=200.
# dt=1.0
# h=1
# hf=1
# j=1
# jf=1
# #gamma=collect(0.0:0.0005:0.002)
# gamma=1.0
# gamma=collect(0.01:0.05:0.42)
t_dt=collect(0:dt:tdt)
t_vals=collect(0:dt:t_max);
######################### Spin Part ####################

# if Nu < 12 

#     N=Nu
#     Nn=N/2;
#     x=[0 1; 1 0]; #Pauli Matrices(x,y,z)
#     y=[0. -im; im 0.];
#     z=[1 0; 0 -1];
#     Ii=(Matrix(1.0*I,2,2)); #Identity Matrix
#     Ising=kron(z,z);#Ising interaction term="Ising"
#     Isingx=kron(x,x);
#     Field=kron(x,Ii)+kron(Ii,x); #On-site field="Field"
#     Is=copy(Ising)
#     for n=3:(Nn)
#         Is=kron(Is,Ii)+kron((Matrix(1.0*I,2^(Int(n-2)),2^(Int(n-2)))),Ising)
#     end
#     Is2=copy(Field)
#     for
#         n=3:Nn
#         Is2=kron(Is2,Ii)+kron((Matrix(1.0*I,2^(Int(n-1)),2^(Int(n-1)))),x)
#     end
#     Hamil=-j*Is-h*Is2
#     Hamilf=-jf*Is-hf*Is2

#     Dm,Um=eigvals(Hamil),eigvecs(Hamil)
#     z1=((kron(z,(Matrix(1.0*I,Int(2^(Nn-1)),Int(2^(Nn-1)))))))
#     x1=((kron(x,(Matrix(1.0*I,Int(2^(Nn-1)),Int(2^(Nn-1)))))))

#     z1z2=((kron(Ising,(Matrix(1.0*I,Int(2^(Nn-2)),Int(2^(Nn-2)))))));

#     zdata=[]
#     rho=Um[:,1]*Um[:,1]';
#     rho=complex(rho);
#     for r=1:length(gamma)
#         if length(gamma)>1
#             row=1
#             t=t_max
#         else
#             row=t_vals
#             t=dt
#         end
#         zz=zeros(length(row),length(t_dt))
#         zz=complex(zz);
#         using DifferentialEquations
#         z2=z1 
#         for m=1:length(row)
#             if m>1
#                 rho=complex(rho)
#                 u0 = rho
#                 tspan = (0.0,t)
#                 f(u,p,t) = -im*(Hamilf*u-u*Hamilf) -(gamma[r])*(u-z1*u*z1)
#                 prob = ODEProblem(f,u0,tspan)
#                 sol=solve(prob)
#                 rho=sol[length(sol)]
#             end
#             z2=z1*rho
#             for n=1:length(t_dt)
#                 zz[m,n]=tr(z1*z2)
#                 #         using DifferentialEquations
#                 z2=complex(z2)
#                 u0=z2
#                 tspan = (0.0,dt)
#                 p(u,p,t) = -im*(Hamilf*u-u*Hamilf)-(gamma[r])*(u-z1*u*z1)
#                 prob = ODEProblem(p,u0,tspan)
#                 sol=solve(prob)
#                 z2=sol[length(sol)]


#             end
#         end
#         push!(zdata,transpose(zz))
#     end
# end
######################### Free Fermion Part ##########################




Hh=zeros(N,N)
Hj=zeros(N,N)
Hd=zeros(N,N)
Hh=zeros(N,N)
Hj=zeros(N,N)
for i=1:2:N
    Hh[i,i+1]=h/2
end
for i=1:2:N-3
    Hj[i,i+3]=j/2
end
for  i=3:1:N
    Hd[1,1]=-2
    Hd[i,i]=-2
end
H1=Hj-Hj'+Hh-Hh'; #H1 is the initial Hamiltonian in lab basis before the Quench.
H1=im*H1
D,U=eigvals(real((H1^2))),eigvecs(real((H1^2)));
sigma=U'*imag.(H1)*U; 
V1=Matrix(1.0*I,N,N)
for i in 2:2:N
    if sigma[i-1,i] < 0
        V1[i,i]=-1
    end
end
Q=U*V1
sigma_corr=Q'*H1*Q;
FCr=zeros(N,N);
FCr=complex(FCr)
for i=1:2:N
    FCr[i,i+1]=-im
    end    
FrCr=FCr+FCr'; # FrCr is the Correlation function in rotated basis before the Quench
FreeCr=Q*FrCr*Q'+Matrix(1.0*I,N,N);
for i=1:2:N
    Hh[i,i+1]=hf/2
end
for i=1:2:N-3
    Hj[i,i+3]=jf/2
end
# xdata=[]
#  for r=1:length(gamma)
    H1=Hh-Hh'+Hj-Hj'; # H1f is the Hamiltonian in the lab basis after the Quench
    H1=im*H1;
    H1=sparse(H1)
#     g=gamma[r]
 g=gamma_m
    H1f=4*im*H1+Hd*g;

    Dt,Ut=eigvals(real.(copy(H1f))),eigvecs(real.(copy(H1f)));
    Dl,Ul=eigvals(real.(copy(transpose(H1f)))),eigvecs(real.(copy(transpose(H1f))));

#     if g>0
        U1=0*Ul
        for n=1:N

            U1[:,n]=Ul[:,(findmax(abs.(Ul[:,n]'*Ut))[2][2])]

        end
        L=inv(round.((U1'*Ut),digits=6))*(U1');
        R=Ut
#     else
#         R=Ut
#         L=Ut';
#     end
   
     #diag(R*L)
    #println(maximum(abs.(H1f-R*diagm(Dt)*L)))

   # if length(gamma)>1
        row=1
        t=t_max
  #  else
#         row=t_vals
#         t=dt
#     end
    x1f=zeros(length(row),length(t_dt))
    x1f=complex(x1f)
    FreeCr2=copy(FreeCr)
    using DifferentialEquations
    A=sparse(im*4*H1)
    P=spzeros(N,N)
    P[2,2]=1
    # println(row)
    for m=1:length(row)
    if m==1
    FreeCr2=copy(FreeCr)
    end
        FreeCr1=copy(FreeCr2)
        FreeCr2=complex(FreeCr2)
        tspan = (0.0,t)
        f(C,p,t) = -(-(A*C-C*A)+2*g*(-2*P*C*P+P*C+C*P))
        prob = ODEProblem(f,FreeCr2,tspan)
        sol=solve(prob)
        FreeCr2=sol[length(sol)]
        for n=1:(length(t_dt))
            x1f[m,n]=FreeCr1[2,2]

            FreeCr1=(exp(H1f*dt)*copy(FreeCr1))


        end
    end
    npzwrite("x1f_$(N)_$(g)_$(tdt)",x1f)
 #    println(maximum(abs.(x1f)))
     #println(g)
    #push!(xdata,transpose(x1f));
#end

